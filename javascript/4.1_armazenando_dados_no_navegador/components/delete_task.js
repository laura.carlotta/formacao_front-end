// componentes = começam com a LetraMaiuscula
const DeleteButton = () => {
    const deleteButton = document.createElement( 'button' );
    deleteButton.insertAdjacentText( 'beforeend', 'Excluir' );

    deleteButton.addEventListener( 'click', deleteTask );

    return deleteButton;
}

const deleteTask = ( event ) => {
    const deleteButton = event.target;
    const taskDeleted = deleteButton.parentElement; // Assim concluimos a tarefa inteira.
    
    taskDeleted.remove();
    return deleteButton;
}

export default DeleteButton;
