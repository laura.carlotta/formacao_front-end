// componentes = começam com a LetraMaiuscula
const DoneButton = () => {
    const donebutton = document.createElement( 'button' );
    donebutton.classList.add( 'check-button' );
    donebutton.insertAdjacentText( 'beforeend', 'Concluido' );

    donebutton.addEventListener( 'click', taskConclude );

    return donebutton;
}

const taskConclude = ( event ) => {
    const doneButton = event.target // o nome dessa constante poderia ser qualquer uma.

    const taskCompleted = doneButton.parentElement;

    taskCompleted.classList.toggle( 'done' );
}

export default DoneButton;