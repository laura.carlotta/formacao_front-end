import DoneButton from './components/conclude_task.js';
import DeleteButton from './components/delete_task.js';

const createTask = ( event ) => {
    event.preventDefault();
    const inputForm = document.querySelector( '[data-form-input]' );
    const taskListUl = document.querySelector( '[data-list]' );
    const inputFormValue = inputForm.value;

    const task = document.createElement( 'li' );
    task.classList.add('task');

    const contentTaskTag = `<p class="content">${ inputFormValue }</p>`;
    task.insertAdjacentHTML( 'beforeend', contentTaskTag ) ;
    task.appendChild( DoneButton() );
    task.appendChild( DeleteButton() );
    taskListUl.appendChild( task );


    inputForm.value = '';
    inputForm.focus();
}

const btnSendForm = document.querySelector( '[data-form-send-button]' );
btnSendForm.addEventListener( 'click', createTask );
