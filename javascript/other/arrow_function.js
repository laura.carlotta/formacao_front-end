// Vídeo explicativo: https://www.youtube.com/watch?v=3EkS9-P3cIM

// FUNÇÃO SIMPLES
/* function sum( number1 , number2 ) {
    return number1 + number2;
}

console.log(sum(12, 25)); */



// ARROW FUNCTION 

const sum = ( number1 , number2 ) => { // arrow function por padrão é anônima, se quisermos dar um nome pra ela é necessário declará-la numa constante;
    return number1 + number2;
}

console.log( sum( 15 , 25 ) );


    // Seria a mesma coisa que isso aqui: 

    const sub = function ( number1 , number2 ) { // tira a palavra function e coloca uma seta
        return number1 - number2;
    }
    console.log( sub( 15 , 25 ) );


    // M U D A N Ç A S

        // 1. Retorna o valor mesmo sem colocar o return (mas precisa retirar os bigodes)
        const multi = ( number1 , number2 ) => number1 * number2;
        console.log(multi(5,12));

        // 2. Sem parametros como fica a sintaxe, como acima, não é necessário colocar o return
        const userName = () => {
            return 'Laura';
        }
        console.log(userName());

        // 3. Só um parâmetro
        const double = number1 => { // Quando tem apenas um parâmetro podemos tirar os parenteses
            return number1 * 2;
        }

        /*      const double = _ => { assim , na teoria não temos necessidade dos parenteses e assim, só com o underscore a function já functiona!
                    return 10 * 2;
                } */
        console.log(double(10));
