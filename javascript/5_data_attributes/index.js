// 1ª coisa, não deixar ela no escopo global
( () => {
    const abas = document.querySelectorAll( '[data-aba]' );

    esconderConteudos = () => {
        const conteudos = document.querySelectorAll( '[data-conteudo]' );
        conteudos.forEach( conteudo => conteudo.classList.add( 'hide' ) );
    }

    inativarAbas = () => {
        abas.forEach( aba => aba.classList.remove( 'ativa') );
    }

    // essa função vai ativar apenas o conteúdo que precisamos, ou seja precisamos passar pra ela qual é esse valor!
    ativarConteudo = ( valor ) => {
        const conteudo = document.querySelector( `[data-conteudo="${valor}"]` );

        conteudo.classList.remove( 'hide' );
    }

    ativarAba = ( aba ) => {
        aba.classList.add( 'ativa' );
    } 

    abas.forEach( aba => aba.addEventListener( 'click' , () => {
        // Quando eu cliacar, quero saber em que aba eu estou.
        const valorDataAba = aba.dataset.aba;
        //                    1     2     3
        // 1. Nosso parâmetro
        // 2. Dataset é um objeto que retorna todas as chave/valor dos datas que nós temos nesse elemento.
        // 3. Sendo um objeto, podemos pegá-lo com o . e o que queremos buscar que é aba.

        esconderConteudos();
        inativarAbas();
        ativarConteudo( valorDataAba );
        ativarAba(aba);
    }));

}) () // <= 2ª coisa: executar ela () a função!