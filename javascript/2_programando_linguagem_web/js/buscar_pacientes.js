let botaoBuscaPaciente = document.getElementById("buscar-pacientes");
botaoBuscaPaciente.addEventListener("click", function() {
    let xhr = new XMLHttpRequest();

    xhr.open("GET", "http://api-pacientes.herokuapp.com/pacientes");

    xhr.addEventListener("load", function () {
        // console.log(typeof xhr.responseText);
        let erroAjax = document.getElementById("erro-ajax");

        if (xhr.status == 200) {
            erroAjax.classList.add("invisivel");
            let respostaPaciente = xhr.responseText;
            // console.log(respostaPaciente);
            // console.log(typeof respostaPaciente);
            
            let listaDePacientesExterna = JSON.parse( respostaPaciente);
            // console.log(listaDePacientesExterna);
            // console.log(typeof listaDePacientesExterna);

            listaDePacientesExterna.forEach( function (pacientes) {
                adicionaPacienteNaTabela( pacientes);
            });
            
            // console.log(listaDePacientesExterna);
            // console.log(typeof listaDePacientesExterna);

        } else {
            console.log( xhr.status);
            console.log( xhr.responseText);
            erroAjax.classList.remove("invisivel");
        }
    });

    xhr.send();

});