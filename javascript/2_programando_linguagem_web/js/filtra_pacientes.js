let campoFiltro = document.getElementById("filtrar-pacientes");

campoFiltro.addEventListener("input", function(){
    // console.log(this.value);

    let listaDePacientes = document.querySelectorAll(".paciente");
    // console.log(listaDePacientes);

    if( this.value.length > 0) {
        for (let i = 0; i < listaDePacientes.length; i++) {
            let paciente = listaDePacientes[i];
            let tdNome = paciente.querySelector(".info-nome");
            let nomeDoPaciente = tdNome.textContent;
            // console.log(nomeDoPaciente);

            let expressaoRegular = new RegExp(this.value, "i");

            if(!expressaoRegular.test(nomeDoPaciente)) {
                paciente.classList.add("invisivel");
            } else {
                paciente.classList.remove("invisivel");
            }
        }
    } else {

        for(let i = 0; i < listaDePacientes.length; i++) {
            let paciente = listaDePacientes[i];
            paciente.classList.remove("invisivel");
        }
    }

    
});
