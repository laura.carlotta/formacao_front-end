const btnAdicionarPaciente = document.querySelector("#adicionar-paciente");
let formInfosUsuario = document.querySelector("#infos-usuario-form");
let ulErro = document.querySelector(".mensagens-erro");

btnAdicionarPaciente.addEventListener("click", function(event) { // funcção anônima, não nomeada
    event.preventDefault(); // "botão, segura seu compotamento padrão e faz o que eu vou te pedir!"

    // extraindo informações do usuário pelo input do form
    let paciente = obtemPacienteDoFormulario(formInfosUsuario);

    let erros = validaPaciente(paciente);
    console.log(erros);

    if(erros.length > 0 ) {

        let msg = setTimeout(exibeMensagensDeErro(erros), 3000);
        return msg;
    }

    adicionaPacienteNaTabela(paciente);
    
    formInfosUsuario.reset();
    formInfosUsuario.nome.focus();
    ulErro.innerHTML = "";
});

function adicionaPacienteNaTabela(paciente) {
    // Cria um novo elemnto tr e td do paciente
    let pacienteTr = montaTr(paciente);
    // Adiciona o paciente a tabela
    let tabela = document.querySelector("#tabela-pacientes");
    tabela.appendChild(pacienteTr);
}

function obtemPacienteDoFormulario(formInfosUsuario) {

    let paciente = {
        nome: formInfosUsuario.nome.value,
        peso: formInfosUsuario.peso.value,
        altura: formInfosUsuario.altura.value,
        gordura: formInfosUsuario.gordura.value,
        imc: calculaIMC(formInfosUsuario.peso.value, formInfosUsuario.altura.value)
    }

    return paciente;
}

function montaTr(paciente) {

    // Cria um novo elemnto tr e td do paciente
    let novoPacienteTr = document.createElement("tr");
    novoPacienteTr.classList.add("paciente");

    // Cria um apendice filho para dicionar a tabela 
    novoPacienteTr.appendChild(montaTd(paciente.nome, "info-nome"));
    novoPacienteTr.appendChild(montaTd(paciente.peso, "info-peso"));
    novoPacienteTr.appendChild(montaTd(paciente.altura, "info-altura"));
    novoPacienteTr.appendChild(montaTd(paciente.gordura, "info-gordura"));
    novoPacienteTr.appendChild(montaTd(paciente.imc, "info-imc"));

    return novoPacienteTr;
}

function montaTd(dado, classe) {
    let td = document.createElement("td");
    td.textContent = dado;
    td.classList.add(classe);

    return td;
}

function validaPaciente(paciente) {

    let listaErros = [];
    
    if(paciente.nome.length == 0 || !isNaN(paciente.nome)){
        listaErros.push("Informe um nome válido!");
    }

    if(!validaPeso(paciente.peso)) 
        listaErros.push("Peso inválido!");

    if(!validaAltura(paciente.altura)) listaErros.push("Altura inválida!");

    if(paciente.gordura.length == 0 || paciente.gordura < 0 || paciente.gordura >= 100 || isNaN(paciente.gordura)) {
        listaErros.push("Gordura inválida!");
    }

    return listaErros;

}

function exibeMensagensDeErro(listaErros) {
    
    ulErro.innerHTML = "";
    
    listaErros.forEach( function(erro) {
        let liMensagem = document.createElement("li");
        liMensagem.textContent = erro;
        ulErro.appendChild(liMensagem);
    });


    /* for (let cont = 0; cont < erros.length; cont++) {
        const erro = erros[cont];
        
    } */
}

// titulo.addEventListener("click", mostraMensagem); <= função nomeada

/* function mostraMensagem() {
    console.log("eu fui clicado!");
} */

/* titulo.addEventListener("click", function() { <= função anônima
    
        ...bloco de código...

}); */
