// remove pacientes

let todosPacientes = document.querySelector("table");

todosPacientes.addEventListener("dblclick", function(event){
    
    event.target.parentNode.classList.add("fade-out");

    setTimeout(function(){
        event.target.parentNode.remove();
    }, 500);
    
    // event.target.remove();

    /* let alvoEvento = event.target;
    let paiDoEvento = alvoEvento.parentNode; // tr (remover a linha toda!)

    paiDoEvento.remove(); */

    // OU DEIXANDO NUMA ÚNICA LINHA:
    // event.target.parentNode.remove();



    // console.log(event.target);
    // console.log(this);
});


/* todosPacientes.forEach(function(paciente){
    paciente.addEventListener("dblclick", function(){
        this.remove();
    });
}); */
