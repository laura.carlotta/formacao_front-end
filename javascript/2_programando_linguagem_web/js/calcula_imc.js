let mainTitulo = document.querySelector(".main-titulo");
mainTitulo.textContent = "Aparecida Nutricionista";


var pacientes = document.querySelectorAll(".paciente");

for(let i = 0; i < pacientes.length; i++) {

    var paciente = pacientes[i];

    let pesoPaciente = pacientes[i].querySelector(".info-peso").textContent;
    let alturaPaciente = pacientes[i].querySelector(".info-altura").textContent;
    let imcPaciente = pacientes[i].querySelector(".info-imc");

    var pesoValido = validaPeso(pesoPaciente);
    let alturaValida = validaAltura(alturaPaciente);

    if (!pesoValido) {
        imcPaciente.textContent = "Peso inválido!";
        pesoValido = false;
        pacientes[i].classList.add("tr-error");
    }

    if (!alturaPaciente) {
        imcPaciente.textContent = "Altura inválida!";
        alturaValida = false;
        pacientes[i].classList.add("tr-error");
    }

    if (pesoValido && alturaValida) {
        var imc = calculaIMC(pesoPaciente, alturaPaciente);
        imcPaciente.textContent = imc;
    }
}

function validaPeso(peso) {
    
    if(peso > 0 && peso <= 500) {
        return true;
    } else {
        return false;
    }
}

function validaAltura(altura) {

    if(altura > 0 && altura <= 3.0) {
        return true;
    } else {
        return false;
    }
}

function calculaIMC(peso, altura) {

    var imc = 0;
    imc = peso / (altura * altura);
    return imc.toFixed(2);
}
