const cpfMask = (e) => {

	let cpfValue = document.querySelector( '[data-type="cpf"]' );
	console.log(cpfValue);

	let widthValue = cpfValue.value.length;

	let keyboard = e.keyCode;

	if ( keyboard != 8 ) { // space
		switch ( widthValue ) {

			case 3:
				cpfValue.value.length = cpfValue.value += '.';
				break;

			case 7:
				cpfValue.value.length = cpfValue.value += '.';
				break;

			case 11:
				cpfValue.value.length = cpfValue.value += '-';
				break;

		}
	}
}

const cepMask = (e) => {
	let cepValue = document.querySelector( '[data-type="cep"]' );
	let widthValue = cepValue.value.length;
	let keyboard = e.keyCode;

	if ( keyboard != 8 ) { // space
		if(widthValue == 5) {
			cepValue.value = cepValue.value += '-';
		}
	}
}

const phoneMask = ( e ) => {
	let phoneValue = document.querySelector( '[data-type="phone"]' );
	let widthValue = phoneValue.value.length;
	let keyboard = e.keyCode;

	if ( keyboard != 8 ) { // space
		switch ( widthValue ) {

			case 1:
				phoneValue.value = '(' + phoneValue.value;
				break;

			case 3:
				phoneValue.value = phoneValue.value + ') ';
				break;

			case 9:
				phoneValue.value = phoneValue.value + '-';
				break;
			case 15:
				let fifteenDigits = 0;
				let fifteenDigitsReplaced = 0;
				phoneValue.value = phoneValue.value.replace( /\D/g, '' );
				fifteenDigits = phoneValue.value;
				fifteenDigitsReplaced = fifteenDigits.replace(/^(\d{2})(\d{5})(\d{4})/ , "($1) $2-$3");
				phoneValue.value = fifteenDigitsReplaced;
		}
	}
}

const onlyNumbers = ( e, dataAttribute ) => {
	let keyboard = e.keyCode;
	let spanMessage = document.querySelector(`${dataAttribute}`);
	spanMessage.innerHTML = '';

	if( keyboard >= 48 && keyboard <= 57 ) {
		spanMessage.innerHTML = '';
		return true;
	} else {
		spanMessage.innerHTML = 'Digite um valor válido.';
		return false;
	}
}
