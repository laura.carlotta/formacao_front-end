export const validityInputs = ( input ) => {
	const inputType = input.dataset.type;

	if ( validations[ inputType ] ) {
		validations[ inputType ]( input );
	}

	if ( input.validity.valid ) {
		input.parentElement.classList.remove( 'input-container--invalido' );
		input.parentElement.querySelector( '.input-mensagem-erro' ).innerHTML = '';
	} else {
		input.parentElement.classList.add( 'input-container--invalido' );
		input.parentElement.querySelector( '.input-mensagem-erro' ).innerHTML = showErrorMessage( inputType, input );
	}
}

const errorTypes = [
	'customError',
	'valueMissing',
	'typeMismatch',
	'patternMismatch'
];

const errorMessages = {
	name: {
		valueMissing: 'O campo nome não pode estar vazio.'
	},

	email: {
		valueMissing: 'O campo email não pode estar vazio.',
		typeMismatch: 'O email digitado não é válido.'
	},

	password: {
		valueMissing: 'O campo senha não pode estar vazio.',
		patternMismatch: 'É necessários que a senha contenha: De 6 a 8 caracteres, 1 letra maiúscula, 1 letra minúscula, 1 número, 1 símbolo.'
	},

	birthdate: {
		valueMissing: 'O campo data de nascimento não pode estar vazio.',
		customError: 'É necessário ser maior de 18 anos para dar seguimento no cadastro.'
	},

	cpf: {
		valueMissing: 'O campo CPF não pode estar vazio.',
		customError: 'O CPF digitado não é válido.'
	},

	cep: {
		valueMissing: 'O Campo CEP não pode estar vazio.',
		patternMismatch: 'Digite um CEP válido.',
		customError: 'CEP inválido.'
	},

	street: {
		valueMissing: 'O campo logradouro não pode estar vazio.',
		customError: 'Digite um valor válido.'
	},

	neighborhood: {
		valueMissing: 'O campo bairro não pode estar vazio.',
		customError: 'Digite um valor válido.'
	},

	city: {
		valueMissing: 'O campo cidade não pode estar vazio.',
		customError: 'Digite um valor válido.'
	},

	state: {
		valueMissing: 'O campo estado não pode estar vazio.',
		customError: 'Digite um valor válido.'
	},

	phone: {
		valueMissing: 'O campo telefone não pode estar vazio.',
		customError: 'Digite um valor válido.'
	},

	price: {
		valueMissing: 'O campo telefone não pode estar vazio.',
		customError: 'Digite um valor válido.'
	}
}

const validations = {
	birthdate: input => birthDateValidation( input ),
	cpf: input => validaCPF( input ),
	cep: input => recuperaCEP( input )
}

/* const birthDate = document.querySelector('[data-type]');

birthDate.addEventListener('blur', (event) => {
	birthDateValidation(event.target);
}); */

const showErrorMessage = ( inputType, input ) => {

	var messages = '';

	errorTypes.forEach( error => {
		if ( input.validity[ error ] ) {
			messages = errorMessages[ inputType ][ error ];
		}
	} );

	return messages;
}

const birthDateValidation = ( input ) => {

	const receivedDate = new Date( input.value );
	let message = '';

	if ( !legalAge( receivedDate ) ) {
		message = 'É necessário ser maior de 18 anos para dar seguimento no cadastro.';
	}

	input.setCustomValidity( message );
}

const legalAge = ( date ) => {
	const atualDate = new Date();
	const datePlusEighteen = new Date( date.getUTCFullYear() + 18, date.getUTCMonth(), date.getUTCDate() );

	return datePlusEighteen <= atualDate;
}

function validaCPF ( input ) {
	const cpfFormatado = input.value.replace( /\D/g, '' );

	console.log( cpfFormatado );
	let mensagem = '';

	if ( !checaCPFRepetido( cpfFormatado ) || !checaEstruturaCPF( cpfFormatado ) ) {
		mensagem = 'O CPF digitado não é válido.'
	}

	input.setCustomValidity( mensagem )
}

function checaCPFRepetido ( cpf ) {
	const valoresRepetidos = [
		'00000000000',
		'11111111111',
		'22222222222',
		'33333333333',
		'44444444444',
		'55555555555',
		'66666666666',
		'77777777777',
		'88888888888',
		'99999999999'
	]
	let cpfValido = true

	valoresRepetidos.forEach( valor => {
		if ( valor == cpf ) {
			cpfValido = false
		}
	} )

	return cpfValido
}

function checaEstruturaCPF ( cpf ) {
	const multiplicador = 10

	return checaDigitoVerificador( cpf, multiplicador )
}

function checaDigitoVerificador ( cpf, multiplicador ) {
	if ( multiplicador >= 12 ) {
		return true
	}

	let multiplicadorInicial = multiplicador
	let soma = 0
	const cpfSemDigitos = cpf.substr( 0, multiplicador - 1 ).split( '' )
	const digitoVerificador = cpf.charAt( multiplicador - 1 )
	for ( let contador = 0; multiplicadorInicial > 1; multiplicadorInicial-- ) {
		soma = soma + cpfSemDigitos[ contador ] * multiplicadorInicial
		contador++
	}

	if ( digitoVerificador == confirmaDigito( soma ) ) {
		return checaDigitoVerificador( cpf, multiplicador + 1 )
	}

	return false
}

function confirmaDigito ( soma ) {
	return 11 - ( soma % 11 )
}


// 123 456 789 09

// let soma = (10 * 1) + (9 * 2) + (8 * 3) ... (2 * 9)
//			  (11 * 1) + (10 * 2) + (9 * 3) ... (2 * 0)

// let digitoVerificador = 11 - (soma % 11)

const recuperaCEP = ( input ) => {
	const cep = input.value.replace( /\D/g, '' );

	const urlViaCep = `http://viacep.com.br/ws/${ cep }/json/`;

	const options = {
		method: 'GET',
		mode: 'cors',
		headers: {
			'content-type': 'application/json;charset=utf-8'
		}
	}

	if ( !input.validity.patternMismatch && !input.validity.valueMissing ) {

		fetch( urlViaCep, options ).then(
			response => response.json()

		).then(
			data => {
				console.log( data );

				if ( data.erro ) {
					input.setCustomValidity( 'CEP inválido.' )
					return
				}

				input.setCustomValidity( '' );

				preencheCamposComCep(data);

				return;
			}
		)
	}
}

const preencheCamposComCep = ( data ) => {
	const street = document.querySelector('[data-type="street"]');
	const neighborhood = document.querySelector('[data-type="neighborhood"]');
	const city = document.querySelector('[data-type="city"]');
	const state = document.querySelector('[data-type="state"]');

	street.value = data.logradouro;
	neighborhood.value = data.bairro;
	city.value = data.localidade;
	state.value = data.uf;
}
