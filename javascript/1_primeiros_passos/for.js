
console.log(`\nTrabalhando com condicionais`);
const listaDeDestinos = new Array(
    `Salvador`,
    `São Paulo`,
    `Rio de Janeiro`
);

const idadeComprador = 18;
const estaAcompanhada = false;
let temPassagemComprada = false;
const destino = `Rio de Janeiro`;

console.log("\nDestinos possíveis:");
console.log(listaDeDestinos);

const podeComprar = idadeComprador >= 18 || estaAcompanhada == true; 

let destinoExiste = false;

for (let cont = 0; cont < 3; cont++) { // mais comum encontrarmos o cont sendo chamado de i (índice);

    if (listaDeDestinos[cont] == destino) {
        console.log("\nDestino existe!");
        destinoExiste = true;
    }
}

console.log("Destino existe: ", destinoExiste)

if (podeComprar && destinoExiste) {
    console.log("Boa viagem!!!");
} else {
    console.log("Desculpe, houve algum erro!\n Tente novamente!");
}

