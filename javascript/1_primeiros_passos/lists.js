
// 1 Trabalhando com listas

const litsOfDestinys = new Array(
    'São Paulo',
    'Salvador',
    'Rio de janeiro'
);
console.log("1", litsOfDestinys);



// 2 Qtde de elementos dentor do array
console.log("2", litsOfDestinys.length); 

    // 3 // 1º elemnto:
        console.log("3", litsOfDestinys[0]);
    // 4 //  ultimo elemento: 
        console.log("4", litsOfDestinys[litsOfDestinys.length - 1]);






console.log("Iterando um array:")
litsOfDestinys.forEach(function(item, index, array){
    console.log(item, index)
})




// 7 // adiciona um item a lista (que se torna o último item da lista)
litsOfDestinys.push("7", 'Florianópolis');




// 10 // adiciona um item a primeira posição doa array
litsOfDestinys.unshift('Porto Alegre');

console.log("Destinos possíveis: ");
console.log(litsOfDestinys);





// 5 // retira um elemento da lista
litsOfDestinys.splice(1,1);
console.log("Lista de destinos atualizados: ");
console.log("5", litsOfDestinys);



// 8 // retira o último elemento especificamente da lista
litsOfDestinys.pop();
console.log("8", litsOfDestinys);



// 9 // retira o primeiro item da lista
litsOfDestinys.shift();



// 11 // retira um elemento especifico da lista
let pos = litsOfDestinys.indexOf("Salvador"); // 
console.log("11", pos)

litsOfDestinys.splice(pos, 1);
console.log("12", litsOfDestinys);




// 6 // mostra o elemento especifico
console.log("6", litsOfDestinys[0]);





/* litsOfDestinys.array.forEach(element => {
    const i = 0;
    litsOfDestinys[i].length == "Rio";
}); */
