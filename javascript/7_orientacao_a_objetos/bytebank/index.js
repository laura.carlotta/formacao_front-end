import { Client } from './Client.js';
import { Account } from './Account.js';

const ricardo = new Client();
ricardo.name = 'Ricardo';
ricardo.cpf = 41356084850;
ricardo.rg = 404631435;

/* console.log(ricardo); */

const ricardoAccount = new Account();
ricardoAccount.agency = 1001;
ricardoAccount.client = ricardo;
ricardoAccount.deposit(350);

/* console.log(ricardoAccount); */


const alice = new Client();
alice.name = 'Alice';
alice.cpf = 44387618833;


/* console.log(alice); */

const aliceAccount = new Account();
aliceAccount.agency = 1001;
aliceAccount.client = alice;

/* console.log(ricardoAccount); */

ricardoAccount.transfer(122, aliceAccount);

console.log(ricardoAccount);
console.log(aliceAccount);
