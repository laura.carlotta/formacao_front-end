export class Account {
	agency;
	client;


	_balance = 0;

	withdraw ( value ) {
		/* saque */
		if ( this._balance >= value ) {
			this._balance -= value;
			return value;
		}
	}

	deposit ( value ) {
		/* deposito */
		if ( value <= 0 ) return;
		this._balance += value;

	}

	transfer ( value, account ) {
		const withDrawValue = this.withdraw( value );
		account.deposit( withDrawValue );
	}
}