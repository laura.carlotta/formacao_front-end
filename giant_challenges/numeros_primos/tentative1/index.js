const button = document.querySelector(".send-button");
let inputNumber = document.getElementById("input1").value;
let inputNumberConverted = Number(inputNumber);
let divAnswer = document.querySelector(".answer");

button.addEventListener("click", function() {
    getPrimeNumber(inputNumberConverted);
    console.log(inputNumberConverted);
    
});

function getPrimeNumber(number) {
    if(number % 2 != 0 || number % 3 != 0 || number % 5 != 0) {
        divAnswer.innerHTML = "Este número É PRIMO!";
        console.log("Entrei aqui");
    } else {
        divAnswer.innerHTML = "Este É UM NÚMERO COMUM";
        console.log("Sai foraaaaa");
    }
}

function validNumber(number) {
    let errorsList = [];

    if(number.length == 0) errorsList.push("Informe um número!");
    if(isNaN(number)) errorsList.push("Só são permitidos números");
    console.log(errorsList);
    return errorsList;
}

function showMessageError(errorsList) {
    errorsList.forEach(function(erro){
        divAnswer.innerHTML = erro;
    });
}