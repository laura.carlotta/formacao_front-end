const buttonSendNumber = document.querySelector(".send-button");
var divAnswer = document.querySelector(".answer");
buttonSendNumber.addEventListener("click", function() {
    var inputNumber = document.getElementById("input1").value;
    
    if(!validationNumber(inputNumber)) {
        if (isPrime(Number(inputNumber))) {
            divAnswer.innerHTML = `<div class="answer true" id="answer">ESSE É UM NÚMERO PRIMO!</div>`;
        } else {
            divAnswer.innerHTML = `<div class="answer" id="answer"><strong><em>Esse é um número comum...</em></strong></div>`;
        }
    }
    console.log(Boolean(inputNumber));
});

function validationNumber(number) {
    if(number == "" || number.indexOf(" ") != -1 || isNaN(Number(number))) {
        divAnswer.innerHTML = "";
        divAnswer.innerHTML = `<div class="answer error" id="answer">Informe um número válido!</div>`
        return true;
    }
}

function isPrime(number) {
    let numberConverted = Number(number);
    if(numberConverted == 2 || numberConverted % 2 != 0)
    if(numberConverted == 3 || numberConverted % 3 != 0) 
    if(numberConverted == 5 || numberConverted % 5 != 0)
    return true;
}
